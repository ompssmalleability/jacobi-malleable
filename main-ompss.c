#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <assert.h>
#include <sys/mman.h>

#define ITERATIONS 1000
#define SIZE 32768
#define MINSHRINK 2
#define MAXEXPAND 64

double fRand(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

void Jacobi_iter(int N, double *aFlat, double *x, double *b, double *xold, int *count, int *displacements, int rows_local, int local_offset) {
    int myrank, rank_size;
    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);

    int i, j, sum1, sum2, i_global;

#pragma omp parallel for private(j, sum1, sum2)
    for (i = 0; i < rows_local; i++) {
        i_global = local_offset + i;
        sum1 = 0.0;
        sum2 = 0.0;
        //if (myrank == 0)
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - %d %d %d\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__, rows_local, i_global, local_offset);

        //for (j = 0; j < N*N; j++)
        //    printf("\t(sergio)[%d/%d] %d: %s(%s,%d) - aFlat[%d]: %d\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__, j, aFlat[j]);
        //MPI_Barrier(MPI_COMM_WORLD);
        for (j = 0; j < i_global; j++) {
            sum1 = sum1 + aFlat[N * i + j] * xold[j];
        }
        //if (myrank == 0)

        //printf("\n\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__);
        for (j = i_global + 1; j < N; j++)
            sum2 = sum2 + aFlat[N * i + j] * xold[j];

        //if (myrank == 0)
        //printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__);

        x[i] = (-sum1 - sum2 + b[i]) / aFlat[N * i + i_global];
        //if (myrank == 0)
        //    printf("\t(sergio)[%d/%d] %d: %s(%s,%d)\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__);
    }
    MPI_Allgatherv(x, rows_local, MPI_DOUBLE, xold, count, displacements, MPI_DOUBLE, MPI_COMM_WORLD);
}

void Jacobi(int N, double *aFlat, double *x, double *b, int maxiter, int iter) {
    int k, myrank, nnodes, rank_size, i;
    double t0, t1;
    MPI_Status status;
    MPI_Comm booster;

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &rank_size);


    //other arrays
    double * xold = (double *) malloc(sizeof (double) * N);
    int * count = (int *) malloc(sizeof (int) * rank_size);
    int * displacements = (int *) malloc(sizeof (int) * rank_size);
    for (i = 0; i < N; i++)
        xold[i] = 1.0;
    for (i = 0; i < rank_size; i++) {
        count[i] = (int) floor(N / rank_size);
        displacements[i] = i * count[i];
    }
    count[rank_size - 1] = N - ((int) floor(N / rank_size))*(rank_size - 1);

    // rows local
    int rows_local = (int) floor(N / rank_size);
    int local_offset = myrank*rows_local;
    if (myrank == (rank_size - 1))
        rows_local = N - rows_local * (rank_size - 1);

    //if (iter % 100 == 0)
    //    if (myrank == 0)
    //        printf("(sergio)[%d/%d] %d: %s(%s,%d) PROCESSING STEP %d\n", myrank, rank_size, getpid(), __FILE__, __func__, __LINE__, iter);

    if (iter < maxiter) {
        //sleep(2);
        t0 = MPI_Wtime();
        Jacobi_iter(N, aFlat, x, b, xold, count, displacements, rows_local, local_offset);
        t1 = MPI_Wtime();
        if (myrank == 0) printf("Iteration %d processed in %g seconds.\n", iter, t1 - t0);
        iter++;

        int action = 0;
        action = dmr_check_status(MINSHRINK, MAXEXPAND, 2, 8, &nnodes, &booster);

        //if (myrank == 0) printf("Action %d to %d nodes\n", action, nnodes);

        if (action <= 0)
            Jacobi(N, aFlat, x, b, maxiter, iter);
        else if (action == 1) {
            int FACTOR = nnodes / rank_size;
            for (int i = 0; i < FACTOR; i++) {
                int dst = myrank * FACTOR + i;
#pragma omp task in(aFlat[0:N*N+1]) in(x[0:N+1]) in(b[0:N+1]) onto(booster, dst)
                Jacobi(N, aFlat, x, b, maxiter, iter);
            }
#pragma omp taskwait
        } else if (action == 2) {
            int FACTOR = rank_size / nnodes;
            if ((myrank % FACTOR) < (FACTOR - 1)) {
            } else {
                int dst = myrank / FACTOR;
#pragma omp task in(aFlat[0:N*N+1]) in(x[0:N+1]) in(b[0:N+1]) onto(booster, dst)
                Jacobi(N, aFlat, x, b, maxiter, iter);
#pragma omp taskwait
            }
        }
    }
    free(xold);
    free(count);
    free(displacements);
}

int main(int argc, char** argv) {
    srand(1);
    double t0, t1;
    int nbproc, myrank, n = SIZE, i;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nbproc);

    double *x, *b;
    double **A;
    double *aFlat;

    t0 = MPI_Wtime();

    //A = (double **) malloc(sizeof (double) * n);
    //for (int i = 0; i < n; i++)
    //    A[i] = (double *) malloc(sizeof (double) * n);

    b = (double *) malloc(sizeof (double) * n);
    x = (double *) malloc(sizeof (double) * n);
    aFlat = (double *) malloc(sizeof (double) * n * n);

    for (i = 0; i < n; i++) {
        b[i] = fRand(0, 1000);
        //x[i] = fRand(0, 1000);
        for (int j = 0; j < n; j++) {
            aFlat[n * i + j] = fRand(0, 1000);
            //A[i][j] = fRand(0, 1000);
        }
    }
    t1 = MPI_Wtime();
    if (myrank == 0) printf("Matrix of %d elements generated in %g seconds.\n", n, t1 - t0);

    Jacobi(n, aFlat, x, b, ITERATIONS, 0);

    //for (i = 0; i < n; i++)
    //    free(A[i]);
    //free(A);
    free(x);
    free(b);
    free(aFlat);

    MPI_Finalize();
    return 0;
}
