#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <mpi.h>
#include <assert.h>
#include <sys/mman.h>

double fRand(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int Jacobi(int mynode, int numnodes, int N, double **A, double *x, double *b, double abstol);

int main(int argc, char** argv) {
    srand(1);
    double t0, t1;
    int nbproc, myrank, n = 10000;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nbproc);

    double *x, *b, abstol = 0.01, k;
    double **A;

    t0 = MPI_Wtime();

    A = (double **) malloc(sizeof (double) * n);
    for (int i = 0; i < n; i++)
        A[i] = (double *) malloc(sizeof (double) * n);

    b = (double *) malloc(sizeof (double) * n);
    x = (double *) malloc(sizeof (double) * n);

    for (int i = 0; i < n; i++) {
        b[i] = fRand(0, 1000);
        //x[i] = fRand(0, 1000);
        for (int j = 0; j < n; j++)
            A[i][j] = fRand(0, 1000);
    }
    t1 = MPI_Wtime();
    if (myrank == 0) printf("Matrix of %d elements generated in %g seconds.\n", n, t1 - t0);

    k = Jacobi(myrank, nbproc, n, A, x, b, abstol);

    for (int i = 0; i < n; i++)
        free(A[i]);
    free(A);
    free(x);
    free(b);

    MPI_Finalize();
    return 0;
}

int Jacobi(int mynode, int numnodes, int N, double **A, double *x, double *b, double abstol) {
    int i, j, k, i_global;
    int maxit = 10000;
    int rows_local, local_offset, last_rows_local;
    int *count, *displacements;
    double sum1, sum2, *xold;
    double error_sum_local, error_sum_global, total_error;
    MPI_Status status;
    rows_local = (int) floor(N / numnodes);
    local_offset = mynode*rows_local;

    if (mynode == (numnodes - 1))
        rows_local = N - rows_local * (numnodes - 1);

    double t0, t1, t2, t3;

    xold = (double *) malloc(sizeof (double) * N);
    count = (int *) malloc(sizeof (int) * numnodes);
    displacements = (int *) malloc(sizeof (int) * numnodes);

    //vendosim supozimin inicial tek te gjithe 1.0
    for (i = 0; i < N; i++) {
        xold[i] = 1.0;
    }

    for (i = 0; i < numnodes; i++) {
        count[i] = (int) floor(N / numnodes);
        displacements[i] = i * count[i];
    }
    count[numnodes - 1] = N - ((int) floor(N / numnodes))*(numnodes - 1);

    //Fillimi I iteracioneve

    for (k = 0; k < maxit; k++) {
        error_sum_local = 0.0;
        t0 = MPI_Wtime();
        for (i = 0; i < rows_local; i++) {
            i_global = local_offset + i;
            sum1 = 0.0;
            sum2 = 0.0;
            for (j = 0; j < i_global; j++)
                sum1 = sum1 + A[i][j] * xold[j];
            for (j = i_global + 1; j < N; j++)
                sum2 = sum2 + A[i][j] * xold[j];
            x[i] = (-sum1 - sum2 + b[i]) / A[i][i_global];
            //cout << "x[" << i_global << "]=" << x[i] << endl;
            error_sum_local += (x[i] - xold[i_global])*(x[i] - xold[i_global]);
            //cout << "Gabimi lokal i reshtit " << i_global << " eshte:" << error_sum_local << endl;
        }
        t1 = MPI_Wtime();
        MPI_Allgatherv(x, rows_local, MPI_DOUBLE, xold, count, displacements, MPI_DOUBLE, MPI_COMM_WORLD);

        t3 = MPI_Wtime();
        //if (k % 1000 == 0)
            if (mynode == 0) printf("Iteration %d processed in %g seconds.\n", k, t3 - t0);
    }

    free(xold);
    free(count);
    free(displacements);

    return maxit;
}
