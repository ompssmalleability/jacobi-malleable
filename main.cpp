#include <iostream>
#include <math.h>
#include <stdio.h>
#include "mpi.h"
#include <cstdlib>

using namespace std;

double fRand(double fMin, double fMax) {
    double f = (double) rand() / RAND_MAX;
    return fMin + f * (fMax - fMin);
}

int Jacobi(int mynode, int numnodes, int N, double **A, double *x, double *b, double abstol);

int main(int argc, char** argv) {
    srand(1);
    double t0, t1;
    int nbproc, myrank, n = 50000;

    MPI_Init(&argc, &argv);

    MPI_Comm_rank(MPI_COMM_WORLD, &myrank);
    MPI_Comm_size(MPI_COMM_WORLD, &nbproc);

    double *x, *b, abstol = 0.01, k;
    double **A;

    t0 = MPI_Wtime();
    A = new double*[n];
    for (int i = 0; i < n; i++)
        A[i] = new double[n];
    b = new double[n];
    x = new double[n];

    for (int i = 0; i < n; i++) {
        b[i] = fRand(0, 1000);
        //x[i] = fRand(0, 1000);
        for (int j = 0; j < n; j++)
            A[i][j] = fRand(0, 1000);
    }
    t1 = MPI_Wtime();
    if (myrank == 0) cout << "Matrix of " << n << " elements generated in " << t1 - t0 << " seconds." << endl;

    k = Jacobi(myrank, nbproc, n, A, x, b, abstol);

    for (int i = 0; i < n; i++)
        delete[] A[i];
    delete[] A;
    delete[] x;
    delete[] b;

    MPI_Finalize();
    return 0;
}

void ReadMatrix(double **A, int n) {
    for (int i = 0; i < n; i++)
        for (int j = 0; j < n; j++) {
            cout << "A[" << i << "][" << j << "] = ";
            cin >> A[i][j];
        }
}

void ReadVector(double *b, int n) {
    for (int i = 0; i < n; i++) {
        cout << "b[" << i << "] = ";
        cin >> b[i];
    }
}

int Jacobi(int mynode, int numnodes, int N, double **A, double *x, double *b, double abstol) {
    int i, j, k, i_global;
    int maxit = 10;
    int rows_local, local_offset, last_rows_local;
    int *count, *displacements;
    double sum1, sum2, *xold;
    double error_sum_local, error_sum_global, total_error;
    MPI_Status status;
    rows_local = (int) floor(N / numnodes);
    local_offset = mynode*rows_local;

    if (mynode == (numnodes - 1))
        rows_local = N - rows_local * (numnodes - 1);

    double t0, t1, t2, t3;

    xold = new double[N];
    count = new int[numnodes];
    displacements = new int[numnodes];

    //vendosim supozimin inicial tek te gjithe 1.0
    for (i = 0; i < N; i++) {
        xold[i] = 1.0;
    }

    for (i = 0; i < numnodes; i++) {
        count[i] = (int) floor(N / numnodes);
        displacements[i] = i * count[i];
    }
    count[numnodes - 1] = N - ((int) floor(N / numnodes))*(numnodes - 1);

    //Fillimi I iteracioneve

    for (k = 0; k < maxit; k++) {
        error_sum_local = 0.0;
        t0 = MPI_Wtime();
        for (i = 0; i < rows_local; i++) {
            i_global = local_offset + i;
            sum1 = 0.0;
            sum2 = 0.0;
            for (j = 0; j < i_global; j++)
                sum1 = sum1 + A[i][j] * xold[j];
            for (j = i_global + 1; j < N; j++)
                sum2 = sum2 + A[i][j] * xold[j];
            x[i] = (-sum1 - sum2 + b[i]) / A[i][i_global];
            //cout << "x[" << i_global << "]=" << x[i] << endl;
            error_sum_local += (x[i] - xold[i_global])*(x[i] - xold[i_global]);
            //cout << "Gabimi lokal i reshtit " << i_global << " eshte:" << error_sum_local << endl;
        }
        t1 = MPI_Wtime();
        MPI_Allgatherv(x, rows_local, MPI_DOUBLE, xold, count, displacements, MPI_DOUBLE, MPI_COMM_WORLD);

        t3 = MPI_Wtime();
        if (mynode == 0) cout << "Iteration " << k << ":" << t3 - t0 << "seconds." << endl;
    }

    delete[] xold;
    delete[] count;
    delete[] displacements;
    return maxit;
}
