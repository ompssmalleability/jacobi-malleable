MPIDIR	    = /state/partition1/soft/gnu/mpich-3.2/
SLURMDIR    = /home/siserte/slurm/

MPIFLAGS    = -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/siserte/slurm/include -L/home/siserte/slurm/lib -lslurm 
OMPSSFLAGS  = -O3 -k --ompss

MPIFLAGS    = -I/home/bsc15/bsc15334/apps/install/mpich-3.2/include -L/home/bsc15/bsc15334/apps/install/mpich-3.2/lib -lmpi
SLURMFLAGS  = -I/home/bsc15/bsc15334/apps/install/slurm/include -L/home/bsc15/bsc15334/apps/install/slurm/lib -lslurm
     
all: jacobi-malleable jacobiC jacobi-moldable
	
######### O (N^2) computation	##########

jacobi-moldable: main-ompss-moldable.c
	mpimcc $(OMPSSFLAGS) $(MPIFLAGS) $(SLURMFLAGS) -fopenmp main-ompss-moldable.c -o jacobimoldable.INTEL64

jacobi-malleable: main-ompss.c
	mpimcc $(OMPSSFLAGS) $(MPIFLAGS) $(SLURMFLAGS) -fopenmp main-ompss.c -o jacobimalleable.INTEL64

jacobiC: main.c
	mpicc -std=gnu99 -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi main.c -o jacobi
	
jacobiCPP: main.cpp
	mpic++ -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi main.cpp
	#g++ -I/state/partition1/soft/gnu/mpich-3.2/include -L/state/partition1/soft/gnu/mpich-3.2/lib -lmpi main.cpp
	
clean:
	rm -f *.out *.o kernels/*.o data/*.out *.dot *.pdf *.intel64 *.mic
