#!/bin/bash

export LD_LIBRARY_PATH=/home/siserte/slurm/lib:/state/partition1/soft/gnu/mpich-3.2/lib:$LD_LIBRARY_PATH

NODELIST="$(scontrol show hostname $SLURM_JOB_NODELIST | paste -d, -s)"
mpiexec -iface eth0 -n $SLURM_JOB_NUM_NODES -hosts $NODELIST ./jacobimalleable.INTEL64

